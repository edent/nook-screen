<?php
	//	Adapted from https://github.com/railalefan/phpOpenLDBWS
	require("OpenLDBWS.php");
	$OpenLDBWS = new OpenLDBWS("");
	$response = $OpenLDBWS->GetDepBoardWithDetails(20, "WOK", "CHX", "to");
	$now = new DateTime();
	$displayDate = $now->format('l H:i');
	header("Content-Type: text/html");

	$template["header"] = "<!doctype html>
	<html>
	<head>
		<meta charset=utf-8>
		<style type='text/css'>
			table {
				border-collapse: collapse;
				width:100%;
			}
			th,td {
				font-family: monospace;
				font-size: 2.25em;
				border: 5px solid #555;
				padding: 10px;
			}
			th:nth-child(1),th:nth-child(2) {
				text-align: left;
			}
			th:nth-child(3),td:nth-child(3) {
				text-align: center;
			}
			th:nth-child(4),td:nth-child(4) {
				text-align: right;
			}
		</style>
	</head>
	<body>
			<table>
				<thead>
					<tr>
						<th colspan='3'><h1>{$displayDate}</h1></th>
					</tr>
					<tr>
						<th>Time</th>
						<th>Expected</th>
						<th>Destination</th>
					</tr>
				</thead>
				<tbody>
			";
		$template["row"] = "
					<tr>
						<td>{std}</td>
				 		<td>{etd}</td>
						<td>{destination}</td>
					</tr>";
		$template["tablefooter"] = "
				</tbody>
			</table>";
		$template["footer"] = "</body>
		</html>";
		if (isset($response->GetStationBoardResult->trainServices->service))
		{
			if (is_array($response->GetStationBoardResult->trainServices->service))
			{
				$services = $response->GetStationBoardResult->trainServices->service;
			}
			else
			{
				$services = array($response->GetStationBoardResult->trainServices->service);
			}
			print $template["header"];
			foreach($services as $service)
			{
				$destinations = array();
				if (is_array($service->destination->location))
				{
					$locations = $service->destination->location;
				} else {
					$locations = array($service->destination->location);
				}

				foreach($locations as $location)
				{
					$destinations[] = $location->locationName;
				}

				//	Is there a platform listed?
				if (isset($service->platform)) {
					//	Ignore Platform 2
					if (2 != $service->platform) {
						$row = $template["row"];

						$std = $service->std;
						$etd = $service->etd;
						$etd_date = new DateTime($now->format('Y-m-d ') . "");

						if ("On time" == $etd) {
							$etd_date = new DateTime($now->format('Y-m-d ') . $std);
						} else if ("Delayed" == $etd) {
							$etd_date = new DateTime($now->format('Y-m-d ') );
						} else {
							$etd_date = new DateTime($now->format('Y-m-d ') . $etd);
						}

						$minutes = $now->diff($etd_date)->i;

						//	Only show trains leaving in more than 10 minutes
						if ($minutes >= 15) {
							$row = str_replace("{std}",$service->std,$row);
							$row = str_replace("{etd}",$service->etd,$row);
							$row = str_replace("{destination}",implode(" and ",$destinations),$row);
							print $row;
						}
					}
				}
			}
			print $template["tablefooter"];
		}

		//	Bus!
		$busURL = "https://api.tfl.gov.uk/StopPoint/940GZZLUCHX/arrivals";

		//  Get the data
		$dataBus = file_get_contents($busURL);

		//  Data is in JSON
		$jsonBus = json_decode($dataBus, true);

		$arrayBus = [];
		foreach ($jsonBus as $buses => $bus) {
		  if ("210" == $bus["lineId"]) {
		    $time = $bus["timeToStation"];
		    $minutes = floor($time / 60);
		    array_push($arrayBus, $minutes);
		  }
		}
		sort($arrayBus);

		$headlineBus = "The 210 bus is in ";
		foreach ($arrayBus as $key => $value) {
		switch ($value) {
			case 0:
				// Too late!
				break;
			case 1:
				// Too late!
				break;
			default:
				$headlineBus .= $value . " minutes. ";
				break;
			}
		}

		print "<h1>{$headlineBus}</h1>";
		print $template["footer"];
