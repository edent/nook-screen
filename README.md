# Nook Screen

Tools and code to turn an old Nook Simple Touch Reader (STR) or Nook Glowlight into a passive display screen.

Details at https://shkspr.mobi/blog/2020/02/turn-an-old-ereader-into-an-information-screen-nook-str/

![An eInk screen stuck to a wall. It displays train times.](https://shkspr.mobi/blog/wp-content/uploads/2020/02/Nook-on-wall.jpeg)

## Original Sources

Most of the software hasn't been updated in years. You can find the latest versions at:

* [Nook Firmware](https://help.barnesandnoble.com/app/answers/detail/a_id/4212#ManualDL) - Barnes and Nobel. Based on Android.
* [Nook Manager](https://github.com/doozan/NookManager/)
* [ReLaunchX](https://github.com/Leszek111/ReLaunchX)
* [Electric Sign](https://github.com/jfriesne/Electric-Sign)
* [PHP library for OpenLDBWS](https://github.com/railalefan/phpOpenLDBWS)
